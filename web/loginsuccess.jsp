<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 25/11/2020
  Time: 09:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="user.database.*"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Panel - LearnEasy</title>
</head>
<body>
<div style="text-align: center">
    <h1>Welcome to LearEasy Website Admin Panel</h1>
    <b>${user.fname} (${user.email})</b>
    <a href="/logout">Logout</a>
    <br><br>
</div>
</body>
</html>
