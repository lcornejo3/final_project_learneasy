package usermanagement.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import usermanagement.bean.UserB;


import javax.swing.*;

public class UserConnect {

    public int registerUser(UserB users) throws ClassNotFoundException {

        String INSERT_USERS_SQL =  "INSERT INTO user" + "  (user_id, fname, lname, email, password) VALUES " + " (?, ?, ?, ?, ?);";

        int result = 0;
        String jdbcUrl = "jdbc:mysql://localhost:3306/hb_client_tracker?serverTimezone=UTC";
        String usr ="hbclient";
        String pass ="hbclient";

        Class.forName("com.mysql.jdbc.Driver");
        try(Connection con = DriverManager.getConnection(jdbcUrl, usr, pass);

            PreparedStatement preparedStatement = con.prepareStatement(INSERT_USERS_SQL)){
            preparedStatement.setInt(1,users.getUser_id());
            preparedStatement.setString(2,users.getFname());
            preparedStatement.setString(3,users.getLname());
            preparedStatement.setString(4,users.getEmail());
            preparedStatement.setString(5,users.getPassword());

            System.out.println(preparedStatement);
            result = preparedStatement.executeUpdate();

        }catch (SQLException e){
            printSQLException(e);
        }
        return result;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
