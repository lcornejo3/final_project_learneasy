package usermanagement.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usermanagement.bean.UserB;
import usermanagement.database.UserConnect;

@WebServlet("/register")
public class UserServlet extends HttpServlet{
    private static final long serialVersionUID =1L;
    private UserConnect userConnect;

    public void init(){
        userConnect = new UserConnect();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{

        String fName = request.getParameter("fname");
        String lName = request.getParameter("lname");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserB userB = new UserB();
        userB.setFname(fName);
        userB.setLname(lName);
        userB.setEmail(email);
        userB.setPassword(password);

        try{
            userConnect.registerUser(userB);
        }catch (Exception e){
            e.printStackTrace();
        }

        response.sendRedirect("userdetails.jsp");

    }
}
