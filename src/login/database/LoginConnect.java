package login.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import login.bean.LoginB;

public class LoginConnect {

   public LoginB checkLogin(String email, String password) throws SQLException, ClassNotFoundException{


        String jdbcUrl = "jdbc:mysql://localhost:3306/hb_client_tracker?serverTimezone=UTC";
        String user ="hbclient";
        String pass ="hbclient";

        Class.forName("com.mysql.jdbc.Driver");

       Connection myConn = DriverManager.getConnection(jdbcUrl, user, pass);
       PreparedStatement preparedStatement = myConn.prepareStatement("SELECT * FROM user where email = ? and password = ?");
       preparedStatement.setString(1, email);
       preparedStatement.setString(2, password);

       ResultSet rs = preparedStatement.executeQuery();

       LoginB loginB = null;

            if(rs.next()){
                loginB = new LoginB();
                loginB.setFname(rs.getString("fname"));
                loginB.setEmail(email);
            }
            myConn.close();
            return loginB;

    }

}
