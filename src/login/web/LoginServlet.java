package login.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import login.bean.LoginB;
import login.database.LoginConnect;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public LoginServlet(){
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        LoginConnect loginConnect = new LoginConnect();

        try {
            LoginB loginB = loginConnect.checkLogin(email,password);
            String destinationPage = "login.jsp";

            if (loginB != null){
                HttpSession session = request.getSession();
                session.setAttribute("user",loginB);
                destinationPage = "loginsuccess.jsp";
            }else {
                String message = "Invalid email/password";
                request.setAttribute("message",message);
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher(destinationPage);
            dispatcher.forward(request,response);

        }catch (SQLException | ClassNotFoundException ex){
            throw new ServletException(ex);
        }
    }
}
